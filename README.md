# Woenderful

Simple CLI-Tool to work with files related to Super Mario Bros. Wonder.

Most of the real magic is done by other libraries, like `roead`.

# About the name

In the level file there's a property on Actors with `ActorHash` `0xB258845B` called "ScrollUpDownType**Woender**InWater".
I just found the presumably typo amusing.
