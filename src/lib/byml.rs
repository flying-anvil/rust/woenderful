use std::{path::Path, error::Error, fs::{File, self}, io::{Read, Seek, Cursor}};

use roead::byml::Byml;

// ===== From BYML =============================================================

pub fn load_byml_from_path(path: impl AsRef<Path>) -> Result<Byml, Box<dyn Error>> {
    let mut file = File::open(&path)?;
    load_byml_from_reader(&mut file)
}

pub fn load_byml_from_bytes(bytes: &[u8]) -> Result<Byml, Box<dyn Error>> {
    let mut cursor = Cursor::new(bytes);
    load_byml_from_reader(&mut cursor)
}

pub fn load_byml_from_reader(reader: &mut (impl Seek + Read)) -> Result<Byml, Box<dyn Error>> {
    let byml = match get_compression(reader)? {
        Compression::None => {
            let mut bytes = Vec::new();
            reader.read_to_end(&mut bytes)?;
            Byml::from_binary(bytes)?
        },
        Compression::Zstd => {
            let mut decompressed = Vec::new();
            let mut decoder = zstd::Decoder::new(reader)?;
            decoder.read_to_end(&mut decompressed)?;
            Byml::from_binary(decompressed)?
        },
    };

    Ok(byml)
}

enum Compression {
    None,
    Zstd,
}

fn get_compression(data: &mut (impl Seek + Read)) -> Result<Compression, Box<dyn Error>> {
    data.seek(std::io::SeekFrom::Start(0))?;

    let mut magic = vec![0; 4];
    data.read_exact(&mut magic)?;
    data.seek(std::io::SeekFrom::Start(0))?;

    let var_name = b"\x28\xB5\x2F\xFD";
    if magic == var_name {
        return Ok(Compression::Zstd);
    }

    Ok(Compression::None)
}

// ===== From YML ==============================================================

pub fn load_byml_from_yml_path(path: impl AsRef<Path>) -> Result<Byml, Box<dyn Error>> {
    let content = fs::read_to_string(path)?;

    let byml = Byml::from_text(content)?;
    Ok(byml)
}

pub fn load_byml_from_yml_text(text: &str) -> Result<Byml, Box<dyn Error>> {
    let byml = Byml::from_text(text)?;
    Ok(byml)
}

pub fn load_byml_from_yml_reader(reader: &mut impl Read) -> Result<Byml, Box<dyn Error>> {
    let mut text = String::new();
    reader.read_to_string(&mut text)?;

    let byml = Byml::from_text(text)?;
    Ok(byml)
}
