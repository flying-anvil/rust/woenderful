use std::process::exit;

use clap::{Parser, Subcommand};

use self::command::{DecodeBymlCommand, EncodeBymlCommand};

mod command;

#[derive(Parser, Debug)]
#[command(propagate_version = true)]
#[command(author, version, about, long_about = None)]
#[command(disable_colored_help = false)]
#[command(
    help_template = "{about}\n\n{usage-heading} {usage} \n\n{all-args}{tab}\n\n\x1B[1;4mAbout:\x1B[0m\n\x1B[3m  Authors: {author-with-newline}  Version: {version}\x1B[0m"
)]
/// Cli utility to do stuff related to SMBW
struct Cli {
    /// What to do
    #[command(subcommand)]
    command: Command,
}

#[derive(Subcommand, Clone, Debug)]
enum Command {
    #[command(name = "decode-byml")]
    /// Converts a byml (binary YAML) file to regular YAML. Automatically decompresses .zs files if needed
    DecodeByml(DecodeBymlCommand),

    #[command(name = "encode-byml")]
    /// Converts a regular YAML to byml (binary YAML)
    EncodeByml(EncodeBymlCommand),
}

pub fn run() {
    let args = Cli::parse();

    #[cfg(debug_assertions)]
    eprintln!("{args:#?}\n");

    let result = match args.command {
        Command::DecodeByml(command) => command.run(),
        Command::EncodeByml(command) => command.run(),
    };

    if let Err(error) = result {
        eprintln!("{error}");
        exit(1);
    }
}
