use std::{error::Error, fs};

use clap::Args;
use woenderful::byml::load_byml_from_path;

#[derive(Args, Clone, Debug)]
pub struct DecodeBymlCommand {
    /// The byml file to convert
    file: String,

    /// Where to store the output
    #[arg(short='o', long="output")]
    output: Option<String>,
}

impl DecodeBymlCommand {
    pub fn run(&self) -> Result<(), Box<dyn Error>> {
        let byml = load_byml_from_path(&self.file)?;

        match &self.output {
            None => println!("{}", byml.to_text()),
            Some(output) => {
                fs::write(output, byml.to_text())?;
            },
        }

        Ok(())
    }
}
