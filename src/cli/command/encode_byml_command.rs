use std::{error::Error, fs::{self, File}, io::Cursor};

use clap::Args;
use woenderful::byml::load_byml_from_yml_path;

#[derive(Args, Clone, Debug)]
pub struct EncodeBymlCommand {
    /// The yml file to convert
    file: String,

    /// Where to store the output
    #[arg(short='o', long="output")]
    output: String,

    /// Use zstd compression for the output. Specify a number to use it as the compression level
    #[arg(short='c', long="compress")]
    compress: Option<Option<u8>>,
}

impl EncodeBymlCommand {
    pub fn run(&self) -> Result<(), Box<dyn Error>> {
        let byml = load_byml_from_yml_path(&self.file)?;
        let binary = byml.to_binary_with_version(roead::Endian::Little, 7);

        match self.compress {
            None => fs::write(&self.output, binary)?,
            Some(level) => {
                let output = File::create(&self.output)?;
                let level = level.unwrap_or(0).clamp(0, 22) as i32;
                // A level of 0 causes zstd to use its default level (which is 3 ATM).
                // A level of 6 seems to be close in size to the original files.

                // eprintln!("Using level {level}");

                let cursor = Cursor::new(binary);
                zstd::stream::copy_encode(cursor, output, level)?;
            },
        }

        Ok(())
    }
}
